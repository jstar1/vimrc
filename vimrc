" Default configured for python
 set shiftwidth=4
 set softtabstop=4
 set tabstop=4
 set expandtab "expand tabs to spaces
 autocmd FileType c setlocal tabstop=8 softtabstop=8 expandtab
 autocmd FileType make setlocal tabstop=8 softtabstop=8 noexpandtab
 syntax on

